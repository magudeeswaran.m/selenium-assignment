package com.assignments;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class OrangeHRM {

	public static void main(String[] args) throws InterruptedException, AWTException {
		
		// Assignment 3
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
        WebElement Username = driver.findElement(By.xpath("//input[@name='username']"));
		
		Username.sendKeys("Admin");
		
		WebElement Password = driver.findElement(By.xpath("//input[@name='password']"));
		
		Password.sendKeys("admin123");
		
		WebElement CliLogin = driver.findElement(By.xpath("//button[text()=' Login ']"));
		
		CliLogin.click();
		
        WebElement CliAdmin = driver.findElement(By.xpath("//span[text()='Admin']"));
		
		CliAdmin.click();
		
		WebElement CliAdd = driver.findElement(By.xpath("//button[text()=' Add ']"));
		
		CliAdd.click();
		
		Thread.sleep(3000);
		
		WebElement Userrole = driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[1]"));
		
		Userrole.click();
		
		Robot r = new Robot();
		
		r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
        r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_ENTER);
		
		r.keyRelease(KeyEvent.VK_ENTER);
		
		r.keyPress(KeyEvent.VK_ESCAPE);
		
		r.keyRelease(KeyEvent.VK_ESCAPE);

        WebElement Status = driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[2]"));
		
		Status.click();
		
        r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
        r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_ENTER);
		
		r.keyRelease(KeyEvent.VK_ENTER);
		
		r.keyPress(KeyEvent.VK_ESCAPE);
		
		r.keyRelease(KeyEvent.VK_ESCAPE);

		
        WebElement Employeename = driver.findElement(By.xpath("//input[@placeholder='Type for hints...']"));
		
		Employeename.sendKeys("h");
		
		Thread.sleep(10000);
		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		
		r.keyPress(KeyEvent.VK_ESCAPE);
		r.keyRelease(KeyEvent.VK_ESCAPE);
		
        driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys("Dhoni Mahi");
		
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("Dhoni@123");
		
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Dhoni@123");
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		
		String checklogin = driver.findElement(By.xpath("//button[text()=' Login ']")).getText();
		
		System.out.println(checklogin);
	}


}

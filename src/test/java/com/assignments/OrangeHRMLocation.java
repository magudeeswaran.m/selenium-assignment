package com.assignments;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeHRMLocation {

	public static void main(String[] args) throws AWTException, InterruptedException {
		
		// Assignment 4
		
        WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
        WebElement Username = driver.findElement(By.xpath("//input[@name='username']"));
		
		Username.sendKeys("Admin");
		
		WebElement Password = driver.findElement(By.xpath("//input[@name='password']"));
		
		Password.sendKeys("admin123");
		
		WebElement CliLogin = driver.findElement(By.xpath("//button[text()=' Login ']"));
		
		CliLogin.click();
		
        WebElement CliAdmin = driver.findElement(By.xpath("//span[text()='Admin']"));
		
		CliAdmin.click();
		
		driver.findElement(By.xpath("//span[text()='Organization ']")).click();
		
		driver.findElement(By.xpath("(//a[@class='oxd-topbar-body-nav-tab-link'])[2]")).click();
		
		driver.findElement(By.xpath("//button[text()=' Add ']")).click();
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("Orange HRM");
		
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[2]")).sendKeys("Bangalore");
		
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[3]")).sendKeys("Tamilnadu");
		
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[4]")).sendKeys("123456");
		
		driver.findElement(By.xpath("//div[@class='oxd-select-text-input']")).click();
		
        Robot r = new Robot();
		
		r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
        r.keyPress(KeyEvent.VK_DOWN);
		
		r.keyRelease(KeyEvent.VK_DOWN);
		
		r.keyPress(KeyEvent.VK_ENTER);
		
		r.keyRelease(KeyEvent.VK_ENTER);
		
		r.keyPress(KeyEvent.VK_ESCAPE);
		
		r.keyRelease(KeyEvent.VK_ESCAPE);
		
		driver.findElement(By.xpath("(//input[@class='oxd-input oxd-input--active'])[6]")).sendKeys("1234567890");
		
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("Nothing");
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
        Thread.sleep(3000);
		
		driver.findElement(By.xpath("//span[@class='oxd-userdropdown-tab']")).click();
		
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
		
		String checklogin = driver.findElement(By.xpath("//button[text()=' Login ']")).getText();
		
		System.out.println(checklogin);
		

	}

}

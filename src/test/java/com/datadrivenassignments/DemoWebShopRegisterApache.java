package com.datadrivenassignments;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.read.biff.BiffException;

public class DemoWebShopRegisterApache {

	public static void main(String[] args) throws BiffException, IOException {
		
		        WebDriver driver = new ChromeDriver();
				
				driver.manage().window().maximize();
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				
				driver.get("https://demowebshop.tricentis.com/register");
				
				File f = new File("/home/magudeeswaran/Documents/Test_data/RegisterDemo.xlsx");
				FileInputStream fis = new FileInputStream(f);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);
				XSSFSheet sheet = workbook.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();
				for(int i=1;i<rows;i++)
				{
					String firstname = sheet.getRow(i).getCell(0).getStringCellValue();
					String lastname = sheet.getRow(i).getCell(1).getStringCellValue();
					String email = sheet.getRow(i).getCell(2).getStringCellValue();
					String password = sheet.getRow(i).getCell(3).getStringCellValue();
					
					
					driver.findElement(By.id("FirstName")).sendKeys(firstname);
					
					driver.findElement(By.id("LastName")).sendKeys(lastname);
					
					driver.findElement(By.id("Email")).sendKeys(email);
					
					driver.findElement(By.id("Password")).sendKeys(password);
					
					driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
				
					driver.navigate().refresh();
				
		        }
		        

			}
	
	
}

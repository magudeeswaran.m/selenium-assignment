package com.datadrivenassignments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DemoWebShopRegisterJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
		
        WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://demowebshop.tricentis.com/register");
		
		File f = new File("/home/magudeeswaran/Documents/Test_data/register1.xls");   
        FileInputStream fis = new FileInputStream(f);
        Workbook book = Workbook.getWorkbook(fis);
        Sheet sh = book.getSheet("Sheet1");
        
        int rows = sh.getRows();
        int columns = sh.getColumns();
        
        for(int i=1;i<rows;i++)
        {      	
        String firstname = sh.getCell(0, i).getContents();
        String lastname = sh.getCell(1, i).getContents();
        String email = sh.getCell(2, i).getContents();
        String password = sh.getCell(3, i).getContents();
        
        
        driver.findElement(By.id("FirstName")).sendKeys(firstname);
		
		driver.findElement(By.id("LastName")).sendKeys(lastname);
		
		driver.findElement(By.id("Email")).sendKeys(email);
		
		driver.findElement(By.id("Password")).sendKeys(password);
		
		driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
	
		driver.navigate().refresh();
		
        }

	}


	}



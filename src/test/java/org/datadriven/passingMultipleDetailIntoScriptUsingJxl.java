package org.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class passingMultipleDetailIntoScriptUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
        WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://demowebshop.tricentis.com/login");
		
		File f = new File("/home/magudeeswaran/Downloads/Userdata1.xls");
        FileInputStream fis = new FileInputStream(f);
        Workbook book = Workbook.getWorkbook(fis);
        Sheet sh = book.getSheet("Sheet1");
        
        int rows = sh.getRows();
        int columns = sh.getColumns();
        
        for(int i=1;i<rows;i++)
        {      	
        String username = sh.getCell(0, i).getContents();
        String password = sh.getCell(1, i).getContents();
        
        
        WebElement login = driver.findElement(By.linkText("Log in"));
		login.click();
        
        driver.findElement(By.id("Email")).sendKeys(username);
		
		driver.findElement(By.id("Password")).sendKeys(password);
		
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		driver.findElement(By.xpath("//a[text()='Log out']")).click();
		
        }

	}

}
